const express = require('express');
const bodyParser = require('body-parser');

const mongoose = require('mongoose');

const BookingSlot = require('./models/booking-slot');
const BookingSlots = require('./models/booking-slots');

const app = express();

mongoose.connect('mongodb+srv://admin:0q5n8LyJLGhfhe7E@hoolycluster.z4yah.mongodb.net/booking-slots?retryWrites=true&w=majority')
  .then(() => {
    console.log('connected to database');
  })
  .catch(err => {
    console.log('connection to database failed error ' + err);
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin','*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
  next();
});

app.post('/api/booking-slots', (req, res, next) => {
  var bookingSlots;
  var bookingSlot7;

  const bookingSlot1 = new BookingSlot({
    number: req.body.bookingSlots[0].number,
    reserved: req.body.bookingSlots[0].reserved,
    userMail: req.body.bookingSlots[0].userMail,
    dateBooked: req.body.bookingSlots[0].dateBooked
  });
  const bookingSlot2 = new BookingSlot({
    number: req.body.bookingSlots[1].number,
    reserved: req.body.bookingSlots[1].reserved,
    userMail: req.body.bookingSlots[1].userMail,
    dateBooked: req.body.bookingSlots[1].dateBooked
  });
  const bookingSlot3 = new BookingSlot({
    number: req.body.bookingSlots[2].number,
    reserved: req.body.bookingSlots[2].reserved,
    userMail: req.body.bookingSlots[2].userMail,
    dateBooked: req.body.bookingSlots[2].dateBooked
  });
  const bookingSlot4 = new BookingSlot({
    number: req.body.bookingSlots[3].number,
    reserved: req.body.bookingSlots[3].reserved,
    userMail: req.body.bookingSlots[3].userMail,
    dateBooked: req.body.bookingSlots[3].dateBooked
  });
  const bookingSlot5 = new BookingSlot({
    number: req.body.bookingSlots[4].number,
    reserved: req.body.bookingSlots[4].reserved,
    userMail: req.body.bookingSlots[4].userMail,
    dateBooked: req.body.bookingSlots[4].dateBooked
  });
  const bookingSlot6 = new BookingSlot({
    number: req.body.bookingSlots[5].number,
    reserved: req.body.bookingSlots[5].reserved,
    userMail: req.body.bookingSlots[5].userMail,
    dateBooked: req.body.bookingSlots[5].dateBooked
  });
  if(req.body.bookingSlots[6]) {
    bookingSlot7 = new BookingSlot({
      number: req.body.bookingSlots[6].number,
      reserved: req.body.bookingSlots[6].reserved,
      userMail: req.body.bookingSlots[6].userMail,
      dateBooked: req.body.bookingSlots[6].dateBooked
    });
  }

  // if
  if (req.body.bookingSlots[6]) {
    bookingSlots = new BookingSlots({
      bookingSlots: [
        bookingSlot1,
        bookingSlot2,
        bookingSlot3,
        bookingSlot4,
        bookingSlot5,
        bookingSlot6,
        bookingSlot7
      ],
      date: req.body.date
    });
  } else {
    bookingSlots = new BookingSlots({
      bookingSlots: [
        bookingSlot1,
        bookingSlot2,
        bookingSlot3,
        bookingSlot4,
        bookingSlot5,
        bookingSlot6,
        bookingSlot7
      ],
      date: req.body.date
    });
  }

  console.log(bookingSlots);
  bookingSlots.save();
  res.status(201).json({
    message: 'Booking-slots added successfully',
    bookingSlots: bookingSlots
  });
});


app.get('/api/booking-slots/:day',(req, res, next) => {
  // const bookingSlots = {
  //   date: new Date(),
  //   bookingSlots : [
  //     {
  //       number: 1,
  //       reserved: true,
  //       userMail: 'myMail@mail.com',
  //       dateBooked: new Date()
  //     },
  //     {
  //       number: 2,
  //       reserved: true,
  //       userMail: '2@mail.com',
  //       dateBooked: '01/04/2022'
  //     },
  //     {
  //       number: 3,
  //       reserved: false,
  //       userMail: '3@mail.com',
  //       dateBooked: '02/04/2022'
  //     },
  //     {
  //       number: 4,
  //       reserved: false,
  //       userMail: '4@mail.com',
  //       dateBooked: '03/04/2022'
  //     },
  //     {
  //       number: 5,
  //       reserved: true,
  //       userMail: '5@mail.com',
  //       dateBooked: '04/04/2022'
  //     },
  //     {
  //       number: 6,
  //       reserved: false,
  //       userMail: '6@mail.com',
  //       dateBooked: '05/04/2022'
  //     },
  //     {
  //       number: 7,
  //       reserved: true,
  //       userMail: '7@mail.com',
  //       dateBooked: '06/04/2022'
  //     }
  //   ]
  // }
  let day = req.params.day;
  BookingSlots.findOne().where({ date: day })
    .then(documents => {
      console.log(JSON.stringify(documents));
      console.log('day =' + day);
      return res.status(200).json({
        message: 'Booking slots fetched successfully.',
        bookingSlots: documents
      });
    });


});

app.put('/api/booking-slots/:id', (req, res, next) => {

  const bookingSlot1 = new BookingSlot({
    number: req.body.bookingSlots[0].number,
    reserved: req.body.bookingSlots[0].reserved,
    userMail: req.body.bookingSlots[0].userMail,
    dateBooked: req.body.bookingSlots[0].dateBooked
  });
  const bookingSlot2 = new BookingSlot({
    number: req.body.bookingSlots[1].number,
    reserved: req.body.bookingSlots[1].reserved,
    userMail: req.body.bookingSlots[1].userMail,
    dateBooked: req.body.bookingSlots[1].dateBooked
  });
  const bookingSlot3 = new BookingSlot({
    number: req.body.bookingSlots[2].number,
    reserved: req.body.bookingSlots[2].reserved,
    userMail: req.body.bookingSlots[2].userMail,
    dateBooked: req.body.bookingSlots[2].dateBooked
  });
  const bookingSlot4 = new BookingSlot({
    number: req.body.bookingSlots[3].number,
    reserved: req.body.bookingSlots[3].reserved,
    userMail: req.body.bookingSlots[3].userMail,
    dateBooked: req.body.bookingSlots[3].dateBooked
  });
  const bookingSlot5 = new BookingSlot({
    number: req.body.bookingSlots[4].number,
    reserved: req.body.bookingSlots[4].reserved,
    userMail: req.body.bookingSlots[4].userMail,
    dateBooked: req.body.bookingSlots[4].dateBooked
  });
  const bookingSlot6 = new BookingSlot({
    number: req.body.bookingSlots[5].number,
    reserved: req.body.bookingSlots[5].reserved,
    userMail: req.body.bookingSlots[5].userMail,
    dateBooked: req.body.bookingSlots[5].dateBooked
  });
  if(req.body.bookingSlots[6]) {
    bookingSlot7 = new BookingSlot({
      number: req.body.bookingSlots[6].number,
      reserved: req.body.bookingSlots[6].reserved,
      userMail: req.body.bookingSlots[6].userMail,
      dateBooked: req.body.bookingSlots[6].dateBooked
    });
  }

  // if
  if (req.body.bookingSlots[6]) {
    bookingSlots = new BookingSlots({
      bookingSlots: [
        bookingSlot1,
        bookingSlot2,
        bookingSlot3,
        bookingSlot4,
        bookingSlot5,
        bookingSlot6,
        bookingSlot7
      ],
      date: req.body.date,
      _id: req.body._id
    });
  } else {
    bookingSlots = new BookingSlots({
      bookingSlots: [
        bookingSlot1,
        bookingSlot2,
        bookingSlot3,
        bookingSlot4,
        bookingSlot5,
        bookingSlot6
      ],
      date: req.body.date,
      _id: req.body._id
    });
  }
  console.log('update = ', req.body);
  console.log('id  = ', req.params.id);

  BookingSlots.findByIdAndUpdate(req.params.id, bookingSlots)
.then(documents => {
  console.log(JSON.stringify(documents));
  return res.status(200).json([{
    message: 'Booking slots update successfully, id = ' + req.params.id,
    bookingSlots: documents
  }]);
});;

});


app.delete('/api/booking-slots/:id', (req, res, next) => {
  res.status(200).json({message: 'Booking slots deleted successfully'})
});

module.exports = app;
