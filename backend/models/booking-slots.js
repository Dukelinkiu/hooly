const mongoose = require('mongoose');

const BookingSlotsSchema = mongoose.Schema({
  date: { type: String, required: true },
  bookingSlots: { type: Array, required: false }
});


module.exports = mongoose.model('BookingSlots', BookingSlotsSchema);
