const mongoose = require('mongoose');

const BookingSlotSchema = mongoose.Schema({
  number: { type: Number, required: true },
  reserved: { type: Boolean, required: true },
  userMail: { type: String, required: true },
  dateBooked: { type: Date, required: true },
});

module.exports = mongoose.model('BookingSlot', BookingSlotSchema);
