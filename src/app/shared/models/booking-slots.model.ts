import { BookingSlot } from "./booking-slot.model";

export class BookingSlots {
  _id: string;
  bookingSlots: BookingSlot[];
  date: string;
}
