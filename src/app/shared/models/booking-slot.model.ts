export class BookingSlot {
  id?: string;
  number: number;
  reserved: boolean;
  userMail?: string;
  dateBooked?: Date;
}
