import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { DateSelectionModule } from './date-selection/date-selection.module';
import { HeaderModule } from './header/header.module';
import localeFrFr from '@angular/common/locales/fr';
import { FlexLayoutModule } from '@angular/flex-layout';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';

registerLocaleData(localeFrFr);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HeaderModule,
    FlexLayoutModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DateSelectionModule,
    HttpClientModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: "fr-FR" }, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
