import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { DateTime } from 'luxon';
import { Subscription } from 'rxjs';
import { BookingSlot } from '../shared/models/booking-slot.model';
import { BookingSlots } from '../shared/models/booking-slots.model';
import { BookingSlotService } from './booking-slot/booking-slot.service';

@Component({
  selector: 'app-date-selection',
  templateUrl: './date-selection.component.html',
  styleUrls: ['./date-selection.component.scss']
})
export class DateSelectionComponent implements OnInit, OnDestroy {

  minDate: Date;
  maxDate: Date;
  dateTime: any;
  panelOpenState = false;
  selectedDate: DateTime | undefined;
  templateDate: Date;
  slots: BookingSlots | undefined;
  reservedSlot: BookingSlot | undefined;

  private bsSub: Subscription;

  constructor(private bookingSlotsService: BookingSlotService) {
    const currentYear = new Date().getFullYear();

    this.minDate = new Date(new Date().setDate(new Date().getDate()+1));
    this.maxDate = new Date(currentYear + 1, 11, 31);
  }

  ngOnInit(): void {
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.reservedSlot = undefined;
    this.panelOpenState = true;
    this.slots = undefined;

    this.dateTime = DateTime;
    this.selectedDate = DateTime.fromJSDate(new Date(`${event.value}`));
    this.templateDate = new Date(`${event.value}`);
    const dt = this.dateTime.fromObject({
      weekYear: this.selectedDate.year,
      weekNumber: this.selectedDate.weekNumber
    });



    const dateFromStr = dt.startOf('week');
    const dateToStr = dt.endOf('week');
    console.log('selectDate', this.selectedDate.toLocaleString); // DT

    this.bookingSlotsService.getBookingSlots(this.selectedDate.toFormat('MM-dd-yyyy'));

    this.bsSub = this.bookingSlotsService.getBookingSlotsUpdated()
    .subscribe((bs) => {
      this.slots = bs;
      console.log('bs', bs); // DT
      console.log('slots', this.slots); // DT
      if (this.slots == undefined) {
        const newSlots = this.bookingSlotsService.createNewSlots(this.selectedDate);
        this.bookingSlotsService.addBookingSlot(newSlots);
        if (this.selectedDate) {
          this.bookingSlotsService.getBookingSlots(this.selectedDate.toFormat('MM-dd-yyyy'));
        }
        this.bsSub = this.bookingSlotsService.getBookingSlotsUpdated()
        .subscribe((bs) => {
          this.slots = bs;
          console.log('slots', this.slots); // DT
        });
      }
    });



   //this.reinitializeVariables();
  }

  //changer la façon de tranformer slots
  bookSlot(slot: BookingSlot) {
    if (!slot.reserved) {
      this.reservedSlot = slot;
    }
  }

  reinitializeVariables() {
    this.reservedSlot = undefined;
    // this.panelOpenState = false;

    // this.dateTime = undefined;
    this.selectedDate = undefined;
    this.slots = undefined;
  }

  confirmMethod(date: DateTime | undefined): void {
    if (date) {
      const formatDate = DateTime.fromISO(date.toString());
      if(confirm("Voulez-vous confirmer pour le " + date.toLocaleString() + ' ?')) {
        if (this.reservedSlot && this.slots) {
          this.slots.bookingSlots[this.reservedSlot.number - 1].reserved = true;
          this.slots.bookingSlots[this.reservedSlot.number - 1].dateBooked = new Date();
          if(this.selectedDate) {
            console.log('this.slots dans update = ', this.slots);
            this.slots.date = this.selectedDate.toFormat('MM-dd-yyyy');
            this.bookingSlotsService.update(this.slots);
            this.reinitializeVariables();
            console.log("ConfirmMethod ok");
          }
        }
      }
    }
  }

  ngOnDestroy () {
    this.bsSub.unsubscribe();
  }
}
