import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateSelectionComponent } from './date-selection.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { BookingSlotComponent } from './booking-slot/booking-slot.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { BookingSlotService } from './booking-slot/booking-slot.service';


@NgModule({
  declarations: [
    DateSelectionComponent,
    BookingSlotComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule
  ],
  exports: [],
  providers:[BookingSlotService]
})
export class DateSelectionModule { }
