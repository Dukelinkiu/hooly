import { Component, Input, OnInit } from '@angular/core';
import { BookingSlot } from 'src/app/shared/models/booking-slot.model';

@Component({
  selector: 'app-booking-slot',
  templateUrl: './booking-slot.component.html',
  styleUrls: ['./booking-slot.component.scss']
})
export class BookingSlotComponent implements OnInit {
  @Input() bs: BookingSlot;

  constructor() { }

  ngOnInit(): void {
  }

}
