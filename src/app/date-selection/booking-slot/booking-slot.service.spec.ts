import { TestBed } from '@angular/core/testing';

import { BookingSlotService } from './booking-slot.service';

describe('BookingSlotService', () => {
  let service: BookingSlotService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BookingSlotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
