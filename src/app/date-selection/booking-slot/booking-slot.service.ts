import { Injectable } from '@angular/core';
import { map, Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BookingSlots } from 'src/app/shared/models/booking-slots.model';
import { DateTime } from 'luxon';
import { BookingSlot } from 'src/app/shared/models/booking-slot.model';

@Injectable({
  providedIn: 'root'
})
export class BookingSlotService {

  private baseURL = 'http://localhost:3000/api/';

  private bookingSlots: BookingSlots;
  private bookingSlotsUpdated = new Subject<BookingSlots>();

  constructor(public http: HttpClient) { }

  getBookingSlots(selectedDate: string): void {
    this.http.get<[{message: string, bookingSlots: BookingSlots}]>(this.baseURL + 'booking-slots/' + selectedDate)
    .subscribe(
      (data: any) => {
        if(data.bookingSlots) {
          this.bookingSlots = data.bookingSlots;
        }
        console.log("Formated get query response = ", data);
        console.log("bookingslots get query response = ", this.bookingSlots);
        this.bookingSlotsUpdated.next(this.bookingSlots);
      }
    );
  }

  getBookingSlotsUpdated(): Observable<BookingSlots> {
    return this.bookingSlotsUpdated.asObservable();
  }

  addBookingSlot(slots: BookingSlots | undefined): void {
    this.http.post<{message: string, bookingSlots: BookingSlots}>(this.baseURL + 'booking-slots', slots)
      .subscribe((data) => {
        console.log('added slots ', data);
        this.bookingSlots = data.bookingSlots;
        this.bookingSlots._id = data.bookingSlots._id;
        console.log('added  this.bookingSlots ',  this.bookingSlots);
        this.bookingSlotsUpdated.next(this.bookingSlots);
      });
  }

  update(slots: BookingSlots): void {
    this.http.put(this.baseURL + 'booking-slots/'+slots._id, slots)
    .subscribe((data) => {
      console.log(data);
      // this.bookingSlots = slots;
      // this.bookingSlotsUpdated.next(this.bookingSlots);
    });;
  }

  createNewSlots (selectedDate: DateTime | undefined): BookingSlots | undefined {
    let availableSlots = [];
    let bookingSlots: BookingSlots | undefined = undefined;
    if (selectedDate) {
      if ( selectedDate.weekday === 5) {
        console.log('weekday', selectedDate.weekday);
       for ( let i = 1; i < 7; i++) {
          let newBooKingSlot = new BookingSlot();
          newBooKingSlot.number = i;
          newBooKingSlot.reserved = false;
          newBooKingSlot.dateBooked = undefined;
          newBooKingSlot.userMail = undefined;
          availableSlots.push(newBooKingSlot);
        }
      } else {
        for ( let i = 1; i <= 7; i++) {
          let newBooKingSlot = new BookingSlot();
          newBooKingSlot.number = i;
          newBooKingSlot.reserved = false;
          newBooKingSlot.dateBooked = undefined;
          newBooKingSlot.userMail = undefined;
          availableSlots.push(newBooKingSlot);
          }
      }
      bookingSlots = new BookingSlots();
      bookingSlots.date = selectedDate.toFormat('MM-dd-yyyy');
      bookingSlots.bookingSlots = availableSlots;
    }

    return bookingSlots;
  }

  DeleteBookingSlots() {

  }
}
