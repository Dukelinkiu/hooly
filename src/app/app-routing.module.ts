import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DateSelectionComponent } from './date-selection/date-selection.component';

const routes: Routes = [
  { path: 'date-selection', component: DateSelectionComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
